```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt update && sudo apt -y install docker-ce
sudo usermod -aG docker $USER
wget https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz
tar xvzf openshift*.tar.gz
cd openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit/
sudo mv oc kubectl /usr/local/bin/

cat << EOF | sudo tee /etc/docker/daemon.json
{
"insecure-registries" : [ "172.30.0.0/16" ]
}
EOF

sudo systemctl restart docker
sudo systemctl status docker
oc cluster up --public-hostname=127.0.0.1

### on a client machine create the ssh tunnel

# physical_host -> vm(openshift-origin)
sudo ssh -L 8443:localhost:8443 -f -N <user>@<openshift_host_machine_ip>
```
