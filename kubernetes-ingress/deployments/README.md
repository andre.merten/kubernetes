## Create a Namespace, a Service Account, the Default Secret and the Customization Config Map.

1. Create a namespace and a service account for the Ingress Controller
```
kubectl apply -f common/ns-and-sa.yaml
```

2. Create a secret with a TLS certificate and a key for the default server in NGINX
```
kubectl apply -f common/default-server-secret.yaml
```

3. Create a config map for customizing NGINX configuration
```
kubectl apply -f common/nginx-config.yaml
```
## Configure RBAC

1. If RBAC is enabled in your cluster, create a cluster role and bind it to the service account, created in Step 1.
```
kubectl apply -f rbac/rbac.yaml
```

### Below are two options for deploying the Ingress controller:
-- Deployment. Use a Deployment if you plan to dynamically change the number of Ingress controller replicas.
-- DaemonSet.  Use a DaemonSet for deploying the Ingress controller on every node or a subset of nodes.

## Create a Deployment

### For NGINX run:
```
kubectl apply -f deployment/nginx-ingress.yaml
```

### For NGINX Plus, run
```
kubectl apply -f deployment/nginx-plus-ingress.yaml\
```

Note: Update the `nginx-plus-ingress.yaml` with the container image that you have built.
Kubernetes will create on Ingress controller pod.

## Create a DaemonSet

### For NGINX Run
```
kubectl apply -f daemon-set/nginx-ingress.yaml
```

For NGINX Plus run
```
kubectl apply -f daemon-set/nginx-plus-ingress.yaml
```

### Note: Update the `nginx-plus-ingress.yaml` with the container image that you have built.
Kubernetes will create an Ingress controller pod on every node of the cluster.


