### Install kubectl on Host machine ** Must be version 1.17.3
```

curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.17.3/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --short
```

### Initialize lxd on Host machine
```
vagrant@k8s-tester:/tmp$ lxd init
Would you like to use LXD clustering? (yes/no) [default=no]:
Do you want to configure a new storage pool? (yes/no) [default=yes]:
Name of the new storage pool [default=default]:
Name of the storage backend to use (btrfs, dir, lvm) [default=btrfs]: dir
Would you like to connect to a MAAS server? (yes/no) [default=no]:
Would you like to create a new local network bridge? (yes/no) [default=yes]:
What should the new bridge be called? [default=lxdbr0]:
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
Would you like LXD to be available over the network? (yes/no) [default=no]:
Would you like stale cached images to be updated automatically? (yes/no) [default=yes]
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]:
```

### Disable swap
```
sudo -i
swapoff -a
exit
```
### Create the k8s profile
```
lxc profile create k8s
```

## Edit the k8s profile and copy the contents of the following into that file
 ```
lxc profile edit k8s

```
## Paste the following:
```
config:
  limits.cpu: "2"
  limits.memory: 2GB
  limits.memory.swap: "false"
  linux.kernel_modules: ip_tables,ip6_tables,netlink_diag,nf_nat,overlay
  raw.lxc: "lxc.apparmor.profile=unconfined\nlxc.cap.drop= \nlxc.cgroup.devices.allow=a\nlxc.mount.auto=proc:rw
    sys:rw"
  security.privileged: "true"
  security.nesting: "true"
description: LXD profile for Kubernetes
devices:
  eth0:
    name: eth0
    nictype: bridged
    parent: lxdbr0
    type: nic
  root:
    path: /
    pool: default
    type: disk
name: k8s
used_by: []
```

## Create the kmaster first
```
lxc launch images:centos/7 kmaster --profile k8s
```

## Create the kworker{1..x} nodes
```
for example 3 worker nodes

lxc launch images:centos/7 kworker1 --profile k8s
lxc launch images:centos/7 kworker2 --profile k8s
lxc launch images:centos/7 kworker3 --profile k8s
```

## Run the "bootstrap-kube.sh" on the kmaster and all the worker nodes
```
cat bootstrap-kube.sh | lxc exec kmaster bash
cat bootstrap-kube.sh | lxc exec kworker1 bash
cat bootstrap-kube.sh | lxc exec kworker2 bash
cat bootstrap-kube.sh | lxc exec kworker3 bash
```

## Create a '.kube' directory in the $USER's home directory on the HOST machine
```
mkdir ~/.kube
```

## Now copy the the admin.conf file from the kmaster container to the host machine. This will allow you to interact with the cluster from the host machine
```
lxc file pull kmaster/etc/kubernetes/admin.conf ~/.kube/config
```

## Verify you are able to interact with cluster. If working correctly you should see the following..
```
vagrant@k8-host:~/kubernetes/lxd-provisioning$ kubectl get nodes
NAME       STATUS   ROLES    AGE   VERSION
kmaster    Ready    master   37m   v1.14.3
kworker1   Ready    <none>   35m   v1.14.3
kworker2   Ready    <none>   34m   v1.14.3
kworker3   Ready    <none>   31m   v1.14.3


vagrant@k8-host:~/kubernetes/lxd-provisioning$ kubectl cluster-info
Kubernetes master is running at https://10.219.192.182:6443
KubeDNS is running at https://10.219.192.182:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

## Overview of containers running
```
vagrant@master-node:~/kubernetes/lxd-provisioning$ lxc list
+----------+---------+------------------------+----------------------------------------------+------------+-----------+
|   NAME   |  STATE  |          IPV4          |                     IPV6                     |    TYPE    | SNAPSHOTS |
+----------+---------+------------------------+----------------------------------------------+------------+-----------+
| kmaster  | RUNNING | 172.17.0.1 (docker0)   | fd42:8d7d:45b5:ceb:216:3eff:feec:4431 (eth0) | PERSISTENT | 0         |
|          |         | 10.244.0.1 (cni0)      |                                              |            |           |
|          |         | 10.244.0.0 (flannel.1) |                                              |            |           |
|          |         | 10.219.192.182 (eth0)  |                                              |            |           |
+----------+---------+------------------------+----------------------------------------------+------------+-----------+
| kworker1 | RUNNING | 172.17.0.1 (docker0)   | fd42:8d7d:45b5:ceb:216:3eff:fecc:7713 (eth0) | PERSISTENT | 0         |
|          |         | 10.244.1.0 (flannel.1) |                                              |            |           |
|          |         | 10.219.192.171 (eth0)  |                                              |            |           |
+----------+---------+------------------------+----------------------------------------------+------------+-----------+
| kworker2 | RUNNING | 172.17.0.1 (docker0)   | fd42:8d7d:45b5:ceb:216:3eff:fe71:6e2d (eth0) | PERSISTENT | 0         |
|          |         | 10.244.2.0 (flannel.1) |                                              |            |           |
|          |         | 10.219.192.190 (eth0)  |                                              |            |           |
+----------+---------+------------------------+----------------------------------------------+------------+-----------+
| kworker3 | RUNNING | 172.17.0.1 (docker0)   | fd42:8d7d:45b5:ceb:216:3eff:fe3f:485f (eth0) | PERSISTENT | 0         |
|          |         | 10.244.3.0 (flannel.1) |                                              |            |           |
|          |         | 10.219.192.161 (eth0)  |                                              |            |           |
+----------+---------+------------------------+----------------------------------------------+------------+-----------+
```






