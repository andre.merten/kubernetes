```
Right, you’ll effectively want:

lxc list
lxc delete <whatever came from list>
lxc image list
lxc image delete <whatever came from list>
lxc network list
lxc network delete <whatever came from list>
echo ‘{“config”: {}}’ | lxc profile edit default
lxc storage volume list default
lxc storage volume delete default <whatever came from list>
lxc profile delete k8s
lxc storage delete default
lxc storage delete default
Effectively listing and the deleting all the objects until everything’s clean.
```
