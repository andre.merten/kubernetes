#### create a privat key for a particular user
```
$ openssl genrsa -out john.key {2048 | 4096}
```


#### create RANDFILE(.rnd) 
```

RANDFILE is used by OpenSSL to store some amount (256 bytes) of seed data from the CSPRNG used internally across invocations. This is particularly useful on low-entropy systems (i.e., embedded devices) that make frequent SSL invocations.

The file is loaded via the function RAND_load_file. Looking at the source, we see that the contents of the file are added to the RNG via RAND_add, so they are in addition to any existing entropy in the RNG.

Since the RNG loads some entropy from system-specific entropy sourcess at the time it is initialized, it seems that RANDFILE is definitely not the only source of entropy used for the RNG state.

Would it confer any security advantage to save output from openssl rand to this file?

That's essentially what OpenSSL does when it says "at exit 256 bytes will be written to it". If you'd like to seed it from something initially, I recommend /dev/urandom, like so:
```

```
dd if=/dev/urandom of=~/.rnd bs=256 count=1
```


#### create a certificate signing request
#### csr == certificate signing request
#### CN is the user, in this case john
#### O indicates that john is a member of the finance group
```
$ openssl req -new -key john.key -out john.csr -subj "/CN=john/O=finance"

```


#### Now in order to sign john's certificate we need the certificate authority (ca.crt) and private key (ca.key) which are stored on the kmaster Node at `/etc/kubernetes/pki`


#### ON THE KMASTER NODE


```
vagrant@dell-r710:~/kubernetes/role_base_access_control$ lxc exec kmaster -- bash
[root@kmaster ~]# cd /etc/kubernetes/pki/
[root@kmaster pki]# ll
total 60
-rw-r--r-- 1 root root 1090 Apr 21 20:37 apiserver-etcd-client.crt
-rw------- 1 root root 1679 Apr 21 20:37 apiserver-etcd-client.key
-rw-r--r-- 1 root root 1099 Apr 21 20:37 apiserver-kubelet-client.crt
-rw------- 1 root root 1675 Apr 21 20:37 apiserver-kubelet-client.key
-rw-r--r-- 1 root root 1220 Apr 21 20:37 apiserver.crt
-rw------- 1 root root 1675 Apr 21 20:37 apiserver.key
-rw-r--r-- 1 root root 1025 Apr 21 20:37 ca.crt
-rw------- 1 root root 1675 Apr 21 20:37 ca.key
drwxr-xr-x 2 root root 4096 Apr 21 20:37 etcd
-rw-r--r-- 1 root root 1038 Apr 21 20:37 front-proxy-ca.crt
-rw------- 1 root root 1679 Apr 21 20:37 front-proxy-ca.key
-rw-r--r-- 1 root root 1058 Apr 21 20:37 front-proxy-client.crt
-rw------- 1 root root 1675 Apr 21 20:37 front-proxy-client.key
-rw------- 1 root root 1679 Apr 21 20:37 sa.key
-rw------- 1 root root  451 Apr 21 20:37 sa.pub

```

#### extract and copy ca.crt and ca.key from kmaster node to host machine

```
$ scp root@kmasterr710/etc/kubernetes/pki/ca.{crt,key} .

```


#### Now sign john.csr (his request) with the certificate authority and private key from the kmaster for 1 year

```

$ openssl x509 -req -in john.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out john.crt -days 365

```

####  allow the user john to start using the cluster
####  Create the kube config file and send it the user john
####  copy the kube admin config and name is user.kubeconfig (john.kubeconfig) 
####  you will need to replace user, name, current-context, user.name, client-certificate-data, and client-key-data
####  in order to paste the certs you must pass .crt and .key files to base64 and remove any line wrapping with -w0(zero) options
####  cat john.crt | base64 -w0
####  cat john.key | base64 -w0
####  use the following as an example:

```

apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJd01EUXlNVEl3TXpjMU1Gb1hEVE13TURReE9USXdNemMxTUZvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTFZtCkZmNEZJdXI3aEVYZktDMkd6WmxyQTFwV3JyRGVob1gxV3VFS20zUGNySFFqYno4RS9PdmlxL3NyL1lWQ2N4ZmwKbEx5bUJuVExXSE5GWGxDZlMvb0dsMFEyY3l6bWVBTlRkQngrSG1XKzlPYjJ0TGg3M1V0ODR5TGZjMmVyUTd0ZApsRFdZZEttNFpWeE1ZZzFlaURveTFPc0gyOEFZVS9kQTF2d2xNS1FPdjhpcEVkank4dkF1dFFqT2RvRTUvUU4yCk5hNHlVS244MHJsZHhETkFtVlFZSEZUTktXc3k1cnJUV1RaSytpaWRlNXFFbnYrY1M1MGZBYzZMeWE5NHlSbkQKMkcyd3pBWFJVTkJqRUI5RDlPU0ZIbk5kRHFISDErbkdFM0tGcDljNmlwZ3QxbVZ6V3h5ZysycG5vSEgwMC9ZTwoyY1FJRXV0WnpwUGE4WnAyY0JVQ0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFCcHoyT0RmbXNEY2dUaEwvY1JCQ2F1eXBKYXYKL1I1VDRZTGdlR3pKRnBOV1VpS1RTMjRJeEw4V09SVzVhcUkzYnJXbHZFaHNOSE1vbkVXMURPSy9wanhxNEsrUQpEdzBmclFnRVVUL1hzOWpKU0EwdFJqSnQ0RS9iNWRoWWY1SlB6a0dHd2hsZkVvdVNLRDcvNEIxVWNEdDJvTXd5Ck5oYUpINWpWWTNhTnd6S0xXWGlCNld2N1Q4YkRGMlZzY3F6NXZJb3RUQmd6R1ljOERZMXRQZGU1d1RlSzh5K0IKaVU0bldZT2NldXIwVWtHUkVmUGZ6V2p0OThZdW92clU1Myt1R0s3NzRrVkFudW1jRjZPTGYxY1VMUlQ4bVRTZwpDazJVYU1MSFZ4SXJMWitPK0JCMEFkM1M1bWdlbWJlSWREWjM1OWlJeExBUnkyQSt0bng2QUFSV2Vxbz0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=
    server: https://10.240.60.100:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: john
  name: john-kubernetes
current-context: john-kubernetes
kind: Config
preferences: {}
users:
- name: john
  user:
    client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN2VENDQWFVQ0ZDbFNIRmNMM3c3VUJoaDlsdW1pQy9GNmx2YndNQTBHQ1NxR1NJYjNEUUVCQ3dVQU1CVXgKRXpBUkJnTlZCQU1UQ210MVltVnlibVYwWlhNd0hoY05NakF3TkRJM01UY3lOVEl3V2hjTk1qRXdOREkzTVRjeQpOVEl3V2pBaE1RMHdDd1lEVlFRRERBUnFiMmh1TVJBd0RnWURWUVFLREFkbWFXNWhibU5sTUlJQklqQU5CZ2txCmhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBODBkcm5yOHVvb3NOVGIyVzNSTFl1RkNzcUZhMHBtTkMKNWpHUXZDdjd6OVRBbnAvSzRZb1ByZWFuYm4rdldWWk5qa2hkZlFWNXovdkpCTWp5Y1BIRXdxQjIzVWNERkx4egpkSjhxK1prTEhDeXpPSUNrUXM3OGJLOVJCZ21SWHZNWTRKMlpwV05GbHdvRlBWdzg4Qys4aGVkUXhuSWN4ZHgrCjZ5cmpHTDZuVldjbXAvRWVhOFpBckU2UGxNVFUybStUZDJXVnpRTDFxTWIwRys5Vk9LbldwZHNtN1BWK2ZnSHIKbFhYZDVKSWVDcGcrbGs3TE9nVUJwa0Z4QWlaOFBzUFErTHVIT1ovT2haNkpheHVabXJVR0pLTFoyL2g0OWk2QQozTy9qUWIxYzBDVldpM004VThha3ZvaS9naXdrcThzYnJBS3dZODlqdXBhNFFBUHl5bEh5RVFJREFRQUJNQTBHCkNTcUdTSWIzRFFFQkN3VUFBNElCQVFCNnFNZlYvUVFUK28vNDBELzhXMXhZOHJ0bUZHSzBKK2hIbnh5WHdPTWQKZ2NtaUFXTTRyWlZlRW1rTFZwOW1xc3ZYNmVsajFHYUdEOFBVNUZDeHgvbkV1MlY4WmtWODlNdVBCaUgraE1SOAptUThVMzV5M3prY0tuNTR4RG9BYXVLd205RFhMenVOeklseUlSYlowMzNsdFQveXpGY1FYbDFrZW1NeUw0c3JPClBTekZoNnF5THd2bTBRUHpmaFFicVhmM1ZyVEJwR1ZYTzd2YXFoSVFQU3c2RTkxQkIxS3JxOS9ON1JhMU5XMG0KM0pidjVTNzFKRWJpRGNQL0xoZ0hCVUd6NW1pa2hzK1ZoVXdpQTVEN0liZGpRZXlZYzlsYzFjM2d2NDVFYlJEUgpRTkY0b1hYMlZ1dTNCWUltV1dEVE5XcGhjWFdhakFjaytXL0hkOHpWMXRrVgotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    client-key-data: LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb1FJQkFBS0NBUUVBODBkcm5yOHVvb3NOVGIyVzNSTFl1RkNzcUZhMHBtTkM1akdRdkN2N3o5VEFucC9LCjRZb1ByZWFuYm4rdldWWk5qa2hkZlFWNXovdkpCTWp5Y1BIRXdxQjIzVWNERkx4emRKOHErWmtMSEN5ek9JQ2sKUXM3OGJLOVJCZ21SWHZNWTRKMlpwV05GbHdvRlBWdzg4Qys4aGVkUXhuSWN4ZHgrNnlyakdMNm5WV2NtcC9FZQphOFpBckU2UGxNVFUybStUZDJXVnpRTDFxTWIwRys5Vk9LbldwZHNtN1BWK2ZnSHJsWFhkNUpJZUNwZytsazdMCk9nVUJwa0Z4QWlaOFBzUFErTHVIT1ovT2haNkpheHVabXJVR0pLTFoyL2g0OWk2QTNPL2pRYjFjMENWV2kzTTgKVThha3ZvaS9naXdrcThzYnJBS3dZODlqdXBhNFFBUHl5bEh5RVFJREFRQUJBb0lCQUR2bTVSZjBpVDRhc1E3bwpWT0pnaHA1enR0L0lZYmpxamdseVFPb2tobDk1U21pRWZnY2s0SkY0VHRmRmR0NnZnbTg3WUZHdGJINEt5RWQyClYwUDZNL0djWTBnKytRSTlHTXlwZUlKMjdhWmxWMGZkWFJUYU5JdHFhNi9wNFRLWTl4SXZuQ3ArRkIrSnB4cWgKd0p0L2xaTC9Ed09lQzNtb3ZsT1hmczJUL0UzcG1rd2NJS3BHakw3dzVlMzRuSmg1WVZreTFwUENBREx4WUg0VQo4cXRHMEEvQmRZaHFObm1lUFZoUW9ta3lBMnYzQ25uU0lRNHQwUjRuTEFkOUJKclRBb2g1ZXlqc1dxWjVNd3hVCmE1Y1gxZnVZQ3BQc1VxbittNmNpOTFyZzNmVFRXb3dSU1B1OWtnUFJ0OGphMUNnS3BtbmRhOU9XZzFYSE1mMlQKeW5TTEhRVUNnWUVBLzBYbU5Mc3Nla2VlYWM2VElOdW1KZ3pDRzhXNEljaVNFRzlJdzZJc09Ya0VGK3M1eUEzawpOKzdrQmVtWm9ocUpTSUgwYUhMOU5OTEhXblNsT0RCVEc5bFQrOGE3WHlXTnNWQVpvcy9nMlJYWG0wMFRYWXpsCkFqOHJqZXhUMzRMQk1zYmx3aTkwbkwzTVFzSXJ6bVFzblZIZlhTQ1FHTDUxcDJWZVgyUk82VmNDZ1lFQTgvakcKOUUvOENZbmRDNUdVZXZKRk4rRzJSMzNlSkpmenVuTzVHaHg4MU0xQUR4YU5FdUdRcjVDVkVZVzZUWHpnbFZOLwpxeWtuQUExTUZFSUJzMXlrRGR2dkdBdmszc0toMmFiYjArNDdHZnQwZ3lPMzNualVaQlJmbXAxcWZkU284OW53CnNOZFNwMkpLREswdE80REw3K3BjbVlwd3NmUmMrUHRzOU1CbGx0Y0NnWUVBOEJVSWhvMkhMbFlQU2duTDNEcFkKa0M2dG5Mc2ZDejc5Q05IeENIV016RnpobWJGVWZhOTY5WlFMQnZpRjBxek1jdEl1VU5IZERJSTBsZ0ZncG5EawpxU3VEemlkQmNDSDhta2Z3Wkd6SVZzTXhMVkdYSmdQdUdtZlpDYmk3eFZuUFpIY2wvYlR1SXNCcXpGcWNJcGVhClVqZC9NTElBZEJVa3lGUVJKdnhMTHk4Q2dZQlJSdHE0LzRCK2lRQXIzM2J3c285ZXU0SW5OUVp4ZUhISTlzNSsKcFdqczErZjBYdFhtcVdDekxGNk05TmtKQVlZc0dTSWFHbDZVeE5sSWttalFvUmRDU3JmUGNmN3dPelVDTm5WYgptQ3oyVGZBVjFDdG9JU2JZcEhUcEpISWFzMk1Kd2R4T2ZaMWFpQU1WVUFnV242VFJLMmxuK1ZmcjRQV3NPTTZtCmh3WVJCd0ovUUJJQityaWFPZXk0Z3hwbmpXNG5LYVlNR3E5OXF3dkpvQkZtU1l4MUtFM05NQ1dQMi9sd1JHYloKYWlNUTZtTjdQaWxlYlFrSVp0MkxqK3kzWUlNQ3FWK01nY2JsUG9OVVNLZVpha3k1SktZVnR0bjZkWDUrOHRDMAp5a3c1clUzQVl6dVNHODRLQ2wvbTZtKzZvSnpPSjZuWW5PTldJMTFjM3FEMnV5SnBEZz09Ci0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tCg==

```

#### Now test out that john has been authenticated to user cluster

```

vagrant@dell-r710:~/kubernetes/role_base_access_control$ kubectl --kubeconfig john.kubeconfig get pods
Error from server (Forbidden): pods is forbidden: User "john" cannot list resource "pods" in API group "" in the namespace "default"

```

#### Now the user can use the kubectl commands by coping the john.kubeconfig to ~/.kube dir and saving the file as 'config'
#### Notice how the user can use the kubectl command but still has not been granted access to cluster







