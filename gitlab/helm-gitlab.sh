helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --install gitlab gitlab/gitlab \
  --namespace gitlab-ce \
  --timeout 600 \
  --set global.hosts.domain=gitlab.local.io \
  --set global.hosts.externalIP=10.156.81.43 \
  --set certmanager-issuer.email=andre.merten@gmail.com \
  --set certmanager.rbac.create=true \
  --set nginx-ingress.rbac.createRole=true \
  --set prometheus.rbac.create=true \
  --set gitlab-runner.rbac.create=true \
  --set global.edition=ce \
  --set redis.cluster.enabled=true \
  --set gitlab-runner.runners.priviledged=true
  
  
  
echo To Delete Deployment Run: "helm delete gitlab --purge"

echo Don't for to manually delete persistent volume claims, persistent volumes AND the "gitlab-ce" namespace as helm will not auto delete these!!!!

