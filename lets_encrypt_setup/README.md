## Prerequisite 

## 1. K8s Cluster (lxc)
## 2. Setup Helm and Tiller


# Setup

## 3. Nginx Ingress Controller
```
# Change into `/kubernetes/kubernetes-ingress/deployments` directory.

# 1. Create the namespace and service account
$ kubectl create -f common/ns-and-sa.yaml

# 2. Create nginx-ingress default-server-secret
$ kubectl create -f common/default-server-secret.yaml

## Setup Configmap

# ConfigMaps bind configuration files, command-line arguments, environment variables, port numbers, and other configuration artifacts to your Pods' containers and system components at runtime. ConfigMaps enable you to separate your configurations from your Pods and components, which helps keep your workloads portable. This makes their configurations easier to change and manage, and prevents hardcoding configuration data to Pod specifications.

ConfigMaps are useful for storing and sharing non-sensitive, unencrypted configuration information. To use sensitive information in your clusters, you must use Secrets.

$  kubectl create -f common/nginx-config.yaml 

# ingress clusterroles and clusterrolebindings
$ kubectl create -f rbac/rbac.yaml 

# nginx-ingress daemon-set
$ kubectl create -f daemon-set/nginx-ingress.yaml

######################################################

kubectl -n nginx-ingress get all
NAME                      READY   STATUS    RESTARTS   AGE
pod/nginx-ingress-4cnsb   1/1     Running   0          15d
pod/nginx-ingress-4ztjb   1/1     Running   0          15d
pod/nginx-ingress-8kvz6   1/1     Running   0          15d
pod/nginx-ingress-fmzmn   1/1     Running   0          15d
pod/nginx-ingress-hc4mx   1/1     Running   0          15d
pod/nginx-ingress-hpplc   1/1     Running   0          15d
pod/nginx-ingress-qmllk   1/1     Running   0          15d

NAME                           DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/nginx-ingress   7         7         7       7            7           <none>          15d


######################################################

```

## 4. Haproxy Load Balancer
```
1.  Create a lxc container for haproxy
2. edit the `/etc/haproxy/haproxy.cfg

#####################################################################

[root@haproxy haproxy]# cat /etc/haproxy/haproxy.cfg
#---------------------------------------------------------------------
# Example configuration for a possible web application.  See the
# full configuration options online.
#
#   http://haproxy.1wt.eu/download/1.4/doc/configuration.txt
#
#---------------------------------------------------------------------

#---------------------------------------------------------------------
# Global settings
#---------------------------------------------------------------------
global
    # to have these messages end up in /var/log/haproxy.log you will
    # need to:
    #
    # 1) configure syslog to accept network log events.  This is done
    #    by adding the '-r' option to the SYSLOGD_OPTIONS in
    #    /etc/sysconfig/syslog
    #
    # 2) configure local2 events to go to the /var/log/haproxy.log
    #   file. A line like the following can be added to
    #   /etc/sysconfig/syslog
    #
    #    local2.*                       /var/log/haproxy.log
    #
    log         127.0.0.1 local2

    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon

    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats

#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------
defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000


frontend http_front
  bind *:443
  mode tcp
  option tcplog
  default_backend http_back

backend http_back
  mode tcp 
  balance roundrobin
  server kube 10.156.81.71:443
  server kube 10.156.81.229:443
  server kube 10.156.81.215:443
  server kube 10.156.81.117:443
  server kube 10.156.81.42:443
  server kube 10.156.81.147:443
  server kube 10.156.81.116:443

###########################################################

*****NOTE******
Mode needs to set to `tcp` because we are installing the Certificates on the worker node and not on the haproxy. TCP creates SSL Tunnel to each worker node.


# Start and enable haproxy service

[root@haproxy haproxy]# systemctl restart haproxy && 

systemctl status haproxy
● haproxy.service - HAProxy Load Balancer
   Loaded: loaded (/usr/lib/systemd/system/haproxy.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-03-12 16:44:28 UTC; 9ms ago
 Main PID: 21781 (haproxy-systemd)
   CGroup: /system.slice/haproxy.service
           ├─21781 /usr/sbin/haproxy-systemd-wrapper -f /etc/haproxy/haproxy.cfg -p /run/haproxy.pid
           ├─21783 /usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg -p /run/haproxy.pid -Ds
           └─21784 /usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg -p /run/haproxy.pid -Ds


# Verfiy the haproxy is listening on port 443

[root@haproxy haproxy]# netstat -nltp

Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:443             0.0.0.0:*               LISTEN      21784/haproxy  
```

## 5. Cert Manager Jetstack
```


## IMPORTANT: you MUST install the cert-manager CRDs **before** installing the
## cert-manager Helm chart.

$ kubectl apply --validate=false \
    -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml

## If you are installing on openshift :
$ oc create \
    -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml

## NOTE: if you are installing the Helm chart into a namespace other than
## 'cert-manager', you **must** replace all occurrences of 'namespace: cert-manager'
## with 'namespace: custom-namespace-name' in the CRDs manifest.
## The below snippet can be used to automate this, replacing
## CUSTOM-NAMESPACE-NAME with the deployment namespace's name:
##
##   kubectl apply --dry-run -o yaml --validate=false \
##      -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml \
##      | sed 's/namespace\: cert-manager/namespace\: CUSTOM-NAMESPACE-NAME/g' \
##      | kubectl apply -f -

## Add the Jetstack Helm repository
$ helm repo add jetstack https://charts.jetstack.io

## Install the cert-manager helm chart
$ helm install --name cert-manager --namespace cert-manager jetstack/cert-manager
```

```
vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ kubectl apply --validate=false \
>     -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml
customresourcedefinition.apiextensions.k8s.io/certificaterequests.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/certificates.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/challenges.acme.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/clusterissuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/issuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/orders.acme.cert-manager.io created
vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ helm repo add jetstack https://charts.jetstack.io
"jetstack" has been added to your repositories
vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ helm install --name cert-manager --namespace cert-manager jetstack/cert-manager
NAME:   cert-manager
LAST DEPLOYED: Mon Mar  9 21:52:51 2020
NAMESPACE: cert-manager
STATUS: DEPLOYED

RESOURCES:
==> v1/ServiceAccount
NAME                     SECRETS  AGE
cert-manager-cainjector  1        0s
cert-manager             1        0s
cert-manager-webhook     1        0s

==> v1beta1/ClusterRole
NAME                                    AGE
cert-manager-cainjector                 0s
cert-manager-controller-clusterissuers  0s
cert-manager-controller-ingress-shim    0s
cert-manager-controller-issuers         0s
cert-manager-controller-orders          0s
cert-manager-controller-challenges      0s
cert-manager-controller-certificates    0s

==> v1/ClusterRole
NAME                                    AGE
cert-manager-view                       0s
cert-manager-edit                       0s
cert-manager-webhook:webhook-requester  0s

==> v1beta1/ClusterRoleBinding
NAME                                    AGE
cert-manager-cainjector                 0s
cert-manager-controller-clusterissuers  0s
cert-manager-controller-ingress-shim    0s
cert-manager-controller-issuers         0s
cert-manager-controller-challenges      0s
cert-manager-controller-certificates    0s
cert-manager-controller-orders          0s
cert-manager-webhook:auth-delegator     0s

==> v1/Service
NAME                  TYPE       CLUSTER-IP      EXTERNAL-IP  PORT(S)   AGE
cert-manager          ClusterIP  10.109.125.168  <none>       9402/TCP  0s
cert-manager-webhook  ClusterIP  10.105.53.208   <none>       443/TCP   0s

==> v1/Deployment
NAME                     DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
cert-manager-cainjector  1        1        1           0          0s
cert-manager             1        1        1           0          0s
cert-manager-webhook     1        1        1           0          0s

==> v1beta1/Role
NAME                                    AGE
cert-manager-cainjector:leaderelection  0s
cert-manager:leaderelection             0s

==> v1beta1/RoleBinding
NAME                                                AGE
cert-manager-cainjector:leaderelection              0s
cert-manager:leaderelection                         0s
cert-manager-webhook:webhook-authentication-reader  0s

==> v1beta1/MutatingWebhookConfiguration
NAME                  AGE
cert-manager-webhook  0s

==> v1beta1/ValidatingWebhookConfiguration
NAME                  AGE
cert-manager-webhook  0s

==> v1/Pod(related)
NAME                                     READY  STATUS             RESTARTS  AGE
cert-manager-cainjector-bfcf448b8-5wq86  0/1    ContainerCreating  0         0s
cert-manager-f8dd564fc-hbbld             0/1    ContainerCreating  0         0s
cert-manager-webhook-7f5bf9cbdf-xdx8m    0/1    ContainerCreating  0         0s


NOTES:
cert-manager has been deployed successfully!

In order to begin issuing certificates, you will need to set up a ClusterIssuer
or Issuer resource (for example, by creating a 'letsencrypt-staging' issuer).

More information on the different types of issuers and how to configure them
can be found in our documentation:

https://docs.cert-manager.io/en/latest/reference/issuers.html

For information on how to configure cert-manager to automatically provision
Certificates for Ingress resources, take a look at the `ingress-shim`
documentation:

https://docs.cert-manager.io/en/latest/reference/ingress-shim.html

vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ kubectl get ns
NAME              STATUS   AGE
cert-manager      Active   27s
default           Active   123m
kube-node-lease   Active   123m
kube-public       Active   123m
kube-system       Active   123m
nginx-ingress     Active   53m

vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ kubectl -n cert-manager get all
NAME                                          READY   STATUS    RESTARTS   AGE
pod/cert-manager-cainjector-bfcf448b8-5wq86   1/1     Running   0          76s
pod/cert-manager-f8dd564fc-hbbld              1/1     Running   0          76s
pod/cert-manager-webhook-7f5bf9cbdf-xdx8m     1/1     Running   0          76s

NAME                           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/cert-manager           ClusterIP   10.109.125.168   <none>        9402/TCP   76s
service/cert-manager-webhook   ClusterIP   10.105.53.208    <none>        443/TCP    76s

NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/cert-manager              1/1     1            1           76s
deployment.apps/cert-manager-cainjector   1/1     1            1           76s
deployment.apps/cert-manager-webhook      1/1     1            1           76s

NAME                                                DESIRED   CURRENT   READY   AGE
replicaset.apps/cert-manager-cainjector-bfcf448b8   1         1         1       76s
replicaset.apps/cert-manager-f8dd564fc              1         1         1       76s
replicaset.apps/cert-manager-webhook-7f5bf9cbdf     1         1         1       76s
vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ kubectl get crds
NAME                                  CREATED AT
certificaterequests.cert-manager.io   2020-03-09T21:50:05Z
certificates.cert-manager.io          2020-03-09T21:50:05Z
challenges.acme.cert-manager.io       2020-03-09T21:50:05Z
clusterissuers.cert-manager.io        2020-03-09T21:50:05Z
issuers.cert-manager.io               2020-03-09T21:50:05Z
orders.acme.cert-manager.io           2020-03-09T21:50:05Z

```

## 6. Cluster Issuer
```
# Change into /kubernetes/yamls/cert-manager-demo directory

# Make sure to use 'apply' as create will fail on ACME server.
$ kubectl apply -f ClusterIssuer.yaml
```
## 7. Nginx Deployment and Service

```
# Create the deployment
$ kubectl create -f nginx-deployment.yaml

# expose the port via port 80
$ kubectl expose deploy nginx-https --port 80

# Note the expose is type "ClusterIP" meaning the service can be accessed via port 80 internally within the cluster, but it does not have any external access.

For external access you can either create a servicetype LoadBalancer or expose the service as a NodePort
```
## 8. Ingress Resource
```
The user will make a request to the load balancer on port 443(https)

The connection between the load balancer and the worker node will be encrypted using TLS. Once the connection has entered the cluster the connection from the ingress-controller to the service nginx will be routed through port 80(http) plain text. The connection within the cluster does not need to be encrypted.  The TLS endpoint in on the worker node on the ingress-controller.

# create the ingress-contoller
$ kubectl create -f ingress-resource.yaml





## 9. DNS Update
## 10. Testing
