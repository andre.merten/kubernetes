#! /bin/bash

deployment_name='nginx-hpa-cpu-demo'
min_reps=1
max_reps=5
cpu_percent=20


kubectl autoscale deploy $deployment_name --min $min_reps --max $max_reps --cpu-percent $cpu_percent
