```
## IMPORTANT: you MUST install the cert-manager CRDs **before** installing the
## cert-manager Helm chart.
$ kubectl apply --validate=false \
    -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml

## If you are installing on openshift :
$ oc create \
    -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml

## NOTE: if you are installing the Helm chart into a namespace other than
## 'cert-manager', you **must** replace all occurrences of 'namespace: cert-manager'
## with 'namespace: custom-namespace-name' in the CRDs manifest.
## The below snippet can be used to automate this, replacing
## CUSTOM-NAMESPACE-NAME with the deployment namespace's name:
##
##   kubectl apply --dry-run -o yaml --validate=false \
##      -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml \
##      | sed 's/namespace\: cert-manager/namespace\: CUSTOM-NAMESPACE-NAME/g' \
##      | kubectl apply -f -

## Add the Jetstack Helm repository
$ helm repo add jetstack https://charts.jetstack.io

## Install the cert-manager helm chart
$ helm install --name my-release --namespace cert-manager jetstack/cert-manager
```

```
vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ kubectl apply --validate=false \
>     -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml
customresourcedefinition.apiextensions.k8s.io/certificaterequests.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/certificates.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/challenges.acme.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/clusterissuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/issuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/orders.acme.cert-manager.io created
vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ helm repo add jetstack https://charts.jetstack.io
"jetstack" has been added to your repositories
vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ helm install --name cert-manager --namespace cert-manager jetstack/cert-manager
NAME:   cert-manager
LAST DEPLOYED: Mon Mar  9 21:52:51 2020
NAMESPACE: cert-manager
STATUS: DEPLOYED

RESOURCES:
==> v1/ServiceAccount
NAME                     SECRETS  AGE
cert-manager-cainjector  1        0s
cert-manager             1        0s
cert-manager-webhook     1        0s

==> v1beta1/ClusterRole
NAME                                    AGE
cert-manager-cainjector                 0s
cert-manager-controller-clusterissuers  0s
cert-manager-controller-ingress-shim    0s
cert-manager-controller-issuers         0s
cert-manager-controller-orders          0s
cert-manager-controller-challenges      0s
cert-manager-controller-certificates    0s

==> v1/ClusterRole
NAME                                    AGE
cert-manager-view                       0s
cert-manager-edit                       0s
cert-manager-webhook:webhook-requester  0s

==> v1beta1/ClusterRoleBinding
NAME                                    AGE
cert-manager-cainjector                 0s
cert-manager-controller-clusterissuers  0s
cert-manager-controller-ingress-shim    0s
cert-manager-controller-issuers         0s
cert-manager-controller-challenges      0s
cert-manager-controller-certificates    0s
cert-manager-controller-orders          0s
cert-manager-webhook:auth-delegator     0s

==> v1/Service
NAME                  TYPE       CLUSTER-IP      EXTERNAL-IP  PORT(S)   AGE
cert-manager          ClusterIP  10.109.125.168  <none>       9402/TCP  0s
cert-manager-webhook  ClusterIP  10.105.53.208   <none>       443/TCP   0s

==> v1/Deployment
NAME                     DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
cert-manager-cainjector  1        1        1           0          0s
cert-manager             1        1        1           0          0s
cert-manager-webhook     1        1        1           0          0s

==> v1beta1/Role
NAME                                    AGE
cert-manager-cainjector:leaderelection  0s
cert-manager:leaderelection             0s

==> v1beta1/RoleBinding
NAME                                                AGE
cert-manager-cainjector:leaderelection              0s
cert-manager:leaderelection                         0s
cert-manager-webhook:webhook-authentication-reader  0s

==> v1beta1/MutatingWebhookConfiguration
NAME                  AGE
cert-manager-webhook  0s

==> v1beta1/ValidatingWebhookConfiguration
NAME                  AGE
cert-manager-webhook  0s

==> v1/Pod(related)
NAME                                     READY  STATUS             RESTARTS  AGE
cert-manager-cainjector-bfcf448b8-5wq86  0/1    ContainerCreating  0         0s
cert-manager-f8dd564fc-hbbld             0/1    ContainerCreating  0         0s
cert-manager-webhook-7f5bf9cbdf-xdx8m    0/1    ContainerCreating  0         0s


NOTES:
cert-manager has been deployed successfully!

In order to begin issuing certificates, you will need to set up a ClusterIssuer
or Issuer resource (for example, by creating a 'letsencrypt-staging' issuer).

More information on the different types of issuers and how to configure them
can be found in our documentation:

https://docs.cert-manager.io/en/latest/reference/issuers.html

For information on how to configure cert-manager to automatically provision
Certificates for Ingress resources, take a look at the `ingress-shim`
documentation:

https://docs.cert-manager.io/en/latest/reference/ingress-shim.html

vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ kubectl get ns
NAME              STATUS   AGE
cert-manager      Active   27s
default           Active   123m
kube-node-lease   Active   123m
kube-public       Active   123m
kube-system       Active   123m
nginx-ingress     Active   53m

vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ kubectl -n cert-manager get all
NAME                                          READY   STATUS    RESTARTS   AGE
pod/cert-manager-cainjector-bfcf448b8-5wq86   1/1     Running   0          76s
pod/cert-manager-f8dd564fc-hbbld              1/1     Running   0          76s
pod/cert-manager-webhook-7f5bf9cbdf-xdx8m     1/1     Running   0          76s

NAME                           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/cert-manager           ClusterIP   10.109.125.168   <none>        9402/TCP   76s
service/cert-manager-webhook   ClusterIP   10.105.53.208    <none>        443/TCP    76s

NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/cert-manager              1/1     1            1           76s
deployment.apps/cert-manager-cainjector   1/1     1            1           76s
deployment.apps/cert-manager-webhook      1/1     1            1           76s

NAME                                                DESIRED   CURRENT   READY   AGE
replicaset.apps/cert-manager-cainjector-bfcf448b8   1         1         1       76s
replicaset.apps/cert-manager-f8dd564fc              1         1         1       76s
replicaset.apps/cert-manager-webhook-7f5bf9cbdf     1         1         1       76s
vagrant@k8-internal:~/kubernetes/yamls/cert-manager-demo$ kubectl get crds
NAME                                  CREATED AT
certificaterequests.cert-manager.io   2020-03-09T21:50:05Z
certificates.cert-manager.io          2020-03-09T21:50:05Z
challenges.acme.cert-manager.io       2020-03-09T21:50:05Z
clusterissuers.cert-manager.io        2020-03-09T21:50:05Z
issuers.cert-manager.io               2020-03-09T21:50:05Z
orders.acme.cert-manager.io           2020-03-09T21:50:05Z

```










```
