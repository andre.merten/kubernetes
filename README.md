# kubernetes
Kubernetes playground


## Prerequisites for Ubuntu 16.04/18.04

Vagrant is a command-line tool for building and managing virtual machine environments. By default, Vagrant can provision machines on top of VirtualBox, Hyper-V, and Docker. Other providers such as Libvirt (KVM), VMware and AWS can be installed via the Vagrant plugin system. Vagrant is typically used by developers to set up a development environment that matches the production environment.

In this tutorial, we’ll show you how to install Vagrant on an Ubuntu 18.04 machine. We’ll be using the VirtualBox provider, which is the default provider for Vagrant. The same steps can be used for Ubuntu 16.04 Xenial Xerus.
Prerequisites

Before continuing with this tutorial, make sure you are logged in as a user with sudo privileges.
Install Vagrant on Ubuntu

To install Vagrant on your Ubuntu system, follow these steps:
### 1. Installing VirtualBox

As mentioned in the introduction, we will provision the machines on top of VirtualBox, so the first step is to [install the VirtualBox package] which is available in the Ubuntu’s repositories:
```
sudo apt install virtualbox
```

If you want to install the latest VirtualBox version from the Oracle repositories, check this tutorial.
### 2. Installing Vagrant

The Vagrant package, which is available in Ubuntu’s repositories, is pretty outdated. We’ll download and install the latest version of Vagrant from the official Vagrant site.
At the time of writing this article, the latest stable version of Vagrant is version 2.2.6. Before continuing with the next steps, check the Vagrant Download page to see if a newer version is available.
Start by updating the package list with:
```
sudo apt update
```

### Download the Vagrant package using the following curl command:
```
curl -O https://releases.hashicorp.com/vagrant/2.2.6/vagrant_2.2.6_x86_64.deb
```
Once the .deb file is downloaded, install it by typing:
```
sudo apt install ./vagrant_2.2.6_x86_64.deb
```
### 3. Verify Vagrant installation

To verify that the installation was successful, run the following command which prints the Vagrant version:
```
vagrant --version
```
The output should look something like this:
```
Vagrant 2.2.6
```

# To Setup up Kubernetes Cluster
```
cd vagrant-provisioning
```

### NOTE: You must modify the following in 'Vagrantfile' for both kmaster and worker nodes
```
v.memory = 5000
v.cpus = 4
NodeCount = 3
```

### To setup cluster run
```
vagrant up
```

## To access/interact with your cluster from HOST MACHINE

#### 1. Add kubernetes Master and Worker Nodes to '/etc/hosts' file on HOST MACHINE to access via DNS
```
# FOR EXAMPLE
# /etc/hosts
...
172.42.42.100 kmaster.example.com  kmaster
172.42.42.101 kworker1.example.com kworker1
172.42.42.102 kworker2.example.com kworker2
172.42.42.103 kworker3.example.com kworker3
...
```

#### 2. ssh into KMASTER NODE and create a root password
```
vagrant ssh kmaster
sudo -i
passwd root
```

#### 3. Exit out of Kmaster Node create .kube dir in home dir of HOST MACHINE
```
mkdir ~/.kube
```
#### 4. Transfer the kubernetes admin config file from Kmaster Node to HOST MACHINE. Enter the root password created in Step 2.
```
scp root@kmaster.example.com:/etc/kubernetes/admin.conf ~/.kube/config
```
#### 5. On HOST MACHINE Verify you are able to interact with the K8 Cluster. NOTE: You may need to install kubectl on HOST MACHINE
```
# To Install Kubectl
$ sudo apt update
$ sudo snap install kubectl --classic

# Example
$ kubectl cluster-info
Kubernetes master is running at https://172.42.42.100:6443
KubeDNS is running at https://172.42.42.100:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```
#### ---------------------------------------------------------------------------------------------------------------------------------------

### Basic Vagrant Commands

Vagrant also mounts the project directory at /vagrant in the virtual machine which allows you to work on your project’s files on your host machine.

### To ssh into the virtual machine, run:
```
vagrant ssh
```
### You can stop the virtual machine with the following command:
```
vagrant halt
```
### The following command stops the machine if it is running, and destroys all resources created during the creation of the machine:
```
vagrant destroy
```

