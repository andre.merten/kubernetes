# Persistent Volume and Claims

1. Create a persistent volume (pv)
2. Create a persistent volume claim (pvc) 
3. Create a pod that uses the pvc -> pv

ReclaimPolicy:
1.Retain(default)
2.Recycle
3.Delete

Access Mode:
1.RWO - Read Write Once -> Volume gets mounted as READ/WRITE to 1 Node the Node with the mount can Read, and Write to the volume. All the other nodes can only Read
2.RWM - Read Write Many -> Volume can be mounted to ANY Node(s) and ALL Nodes can READ/WRITE to the volume.
3.RO -  Read Only -> Volume gets mounted to any node and data can only be read from it.


# Expose Nginx Port as NodePort
$ kubectl expose deploy [nginx-deployment] --port 80 --type NodePort

