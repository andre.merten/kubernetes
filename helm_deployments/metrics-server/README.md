# search for the repo
helm search metics-server

# get the values file from repo to edit
helm inspect values stable/metrics-server > metrics-server.values

