## Create the tiller Service Account
```
kubectl -n kube-system create serviceaccount tiller
```
```
helm init --service-account tiller --override spec.selector.matchLabels.'name'='tiller',spec.selector.matchLabels.'app'='helm' --output yaml | sed 's@apiVersion: extensions/v1beta1@apiVersion: apps/v1@' | kubectl apply -f -
```


## Create the Admin ClusterRole binding for tiller
```
kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
clusterrolebinding.rbac.authorization.k8s.io/tiller created

kubectl get clusterrolebinding tiller
NAME     AGE
tiller   84s
```
## Check the tiller component. Deployment, replicaset, serviceaccount, clusterrolebinding

```
vagrant@k8-internal:~$ kubectl -n kube-system get deploy,replicaset,pod,serviceaccount,clusterrolebinding | grep tiller


deployment.apps/tiller-deploy   1/1     1            1           24m
replicaset.apps/tiller-deploy-68bf6dff8f   1         1         1       24m
pod/tiller-deploy-68bf6dff8f-rdwx5    1/1     Running   0          24m
serviceaccount/tiller                               1         24m
clusterrolebinding.rbac.authorization.k8s.io/tiller 
```

## To Completely delete and remove tiller resources

### Remove tiller deployment
``` 
helm reset --remove-helm-home
```

## Remove the tiller replicaset
```
kubectl -n kube-system delete rs tiller-deploy-<UID>
```

## Remove Clusterrolebinding and Service account
```
kubectl -n kube-system delete clusterrolebinding tiller

kubectl -n kube-system delete serviceaccount tiller

```
