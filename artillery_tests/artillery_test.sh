#! /bin/bash

HOST="ghost.r710.io"
NUM_OF_USERS=50
NUM_OF_REQUESTS=100


# without web report
#DEBUG=http artillery quick --count $NUM_OF_USERS -n $NUM_OF_REQUESTS --insecure https://$HOST

# with web report
DEBUG=http artillery quick --count $NUM_OF_USERS -n $NUM_OF_REQUESTS -o report.json --insecure https://$HOST && artillery report report.json
