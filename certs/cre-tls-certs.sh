# for creating tls self-signed certs


# USER DEFINED
COMMON_NAME='swagger.nodapi.com'
CERT_DIR="/home/vagrant/kubernetes/certs/swagger-nodeapi"
TLS="swagger-nodeapi-com-tls"
NAMESPACE="swagger"

## create openssl tls key and tls certificate
openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout tls.key -out tls.crt -subj "/CN=$COMMON_NAME" -days 365
mkdir -p $CERT_DIR
mv tls.* $CERT_DIR	

kubectl create secret tls $TLS --cert=$CERT_DIR/tls.crt --key=$CERT_DIR/tls.key -n $NAMESPACE
kubectl get secret -n $NAMESPACE

