# create a haproxy lxc container. This haproxy(loadbalancer) is responsible for loadbalancing requests among each one of the worker nodes.
```
lxc launch images:centos/7 haproxy-loadbalancer
```
# view the haproxy-loadbalancer lxc container
```
┌─[andre@optiplex7020]─[~]
└──╼ $lxc list
+----------------------+---------+------------------------+-----------------------------------------------+------------+-----------+
|         NAME         |  STATE  |          IPV4          |                     IPV6                      |    TYPE    | SNAPSHOTS |
+----------------------+---------+------------------------+-----------------------------------------------+------------+-----------+
| haproxy-loadbalancer | RUNNING | 10.148.242.40 (eth0)   | fd42:c95e:3fde:ef51:216:3eff:feb7:3a7b (eth0) | PERSISTENT | 0         |
+----------------------+---------+------------------------+-----------------------------------------------+------------+-----------+
| kmaster              | RUNNING | 172.17.0.1 (docker0)   | fd42:c95e:3fde:ef51:216:3eff:fef6:5473 (eth0) | PERSISTENT | 0         |
|                      |         | 10.244.0.1 (cni0)      |                                               |            |           |
|                      |         | 10.244.0.0 (flannel.1) |                                               |            |           |
|                      |         | 10.148.242.59 (eth0)   |                                               |            |           |
+----------------------+---------+------------------------+-----------------------------------------------+------------+-----------+
| kworker1             | RUNNING | 172.17.0.1 (docker0)   | fd42:c95e:3fde:ef51:216:3eff:fe02:36c1 (eth0) | PERSISTENT | 0         |
|                      |         | 10.244.1.1 (cni0)      |                                               |            |           |
|                      |         | 10.244.1.0 (flannel.1) |                                               |            |           |
|                      |         | 10.148.242.135 (eth0)  |                                               |            |           |
+----------------------+---------+------------------------+-----------------------------------------------+------------+-----------+
| kworker2             | RUNNING | 172.17.0.1 (docker0)   | fd42:c95e:3fde:ef51:216:3eff:fe4c:ddb7 (eth0) | PERSISTENT | 0         |
|                      |         | 10.244.2.1 (cni0)      |                                               |            |           |
|                      |         | 10.244.2.0 (flannel.1) |                                               |            |           |
|                      |         | 10.148.242.151 (eth0)  |                                               |            |           |
+----------------------+---------+------------------------+-----------------------------------------------+------------+-----------+
| kworker3             | RUNNING | 172.17.0.1 (docker0)   | fd42:c95e:3fde:ef51:216:3eff:fe72:ffda (eth0) | PERSISTENT | 0         |
|                      |         | 10.244.3.1 (cni0)      |                                               |            |           |
|                      |         | 10.244.3.0 (flannel.1) |                                               |            |           |
|                      |         | 10.148.242.220 (eth0)  |                                               |            |           |
+----------------------+---------+------------------------+-----------------------------------------------+------------+-----------+
```

# configure the haproxy to loadbalance between each one of the k8s-worker nodes(kworker1..kworker2..etc)
# login to haproxy container to configure
```
lxc exec haproxy-loadbalancer -- bash
```

# install haproxy onto the lxc container
```
yum install -y haproxy
```

# edit haproxy config file --> `/etc/haproxy/haproxy.cfg`
```
yum install -y vim
vim /etc/haproxy/haproxy.cfg
```

# use the following code example and edit `backend http_back` with the ip_address of your k8 worker nodes
# to get the ip_address of your worker nodes run the following command and use the `INTERNAL-IP` value.
```
kubectl get all -o wide

NAME       STATUS   ROLES    AGE     VERSION   INTERNAL-IP      EXTERNAL-IP   OS-IMAGE                KERNEL-VERSION      CONTAINER-RUNTIME
kmaster    Ready    master   4h5m    v1.17.1   10.148.242.59    <none>        CentOS Linux 7 (Core)   4.15.0-91-generic   docker://19.3.5
kworker1   Ready    <none>   3h59m   v1.17.1   10.148.242.135   <none>        CentOS Linux 7 (Core)   4.15.0-91-generic   docker://19.3.5
kworker2   Ready    <none>   3h58m   v1.17.1   10.148.242.151   <none>        CentOS Linux 7 (Core)   4.15.0-91-generic   docker://19.3.5
kworker3   Ready    <none>   3h55m   v1.17.1   10.148.242.220   <none>        CentOS Linux 7 (Core)   4.15.0-91-generic   docker://19.3.5

```

# Replace the config with the following
```
#---------------------------------------------------------------------
# Example configuration for a possible web application.  See the
# full configuration options online.
#
#   http://haproxy.1wt.eu/download/1.4/doc/configuration.txt
#
#---------------------------------------------------------------------

#---------------------------------------------------------------------
# Global settings
#---------------------------------------------------------------------
global
    # to have these messages end up in /var/log/haproxy.log you will
    # need to:
    #
    # 1) configure syslog to accept network log events.  This is done
    #    by adding the '-r' option to the SYSLOGD_OPTIONS in
    #    /etc/sysconfig/syslog
    #
    # 2) configure local2 events to go to the /var/log/haproxy.log
    #   file. A line like the following can be added to
    #   /etc/sysconfig/syslog
    #
    #    local2.*                       /var/log/haproxy.log
    #
    log         127.0.0.1 local2

    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon

    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats

#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------
defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000


# edit this with your k8 worker node ip addresses
frontend http_front
  bind *:80
  stats uri /haproxy?stats
  default_backend http_back

backend http_back
  balance roundrobin
  server kube <worker-node1-ip>:80
  server kube <worker-node2-ip>:80



#---------------------------------------------------------------------
# main frontend which proxys to the backends
#---------------------------------------------------------------------
#frontend  main *:5000
    #acl url_static       path_beg       -i /static /images /javascript /stylesheets
    #acl url_static       path_end       -i .jpg .gif .png .css .js

    #use_backend static          if url_static
    #default_backend             app

#---------------------------------------------------------------------
# static backend for serving up images, stylesheets and such
#---------------------------------------------------------------------
#backend static
    #balance     roundrobin
    #server      static 127.0.0.1:4331 check

#---------------------------------------------------------------------
# round robin balancing between the various backends
#---------------------------------------------------------------------
#backend app
    #balance     roundrobin
    #server  app1 127.0.0.1:5001 check
    #server  app2 127.0.0.1:5002 check
    #server  app3 127.0.0.1:5003 check
    #server  app4 127.0.0.1:5004 check
```

# enable and start haproxy service
```
systemctl enable haproxy && systemctl start haproxy && systemctl status haproxy

[root@haproxy-loadbalancer ~]# systemctl enable haproxy && systemctl start haproxy && systemctl status haproxy
● haproxy.service - HAProxy Load Balancer
   Loaded: loaded (/usr/lib/systemd/system/haproxy.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2020-04-05 20:54:14 UTC; 19s ago
 Main PID: 418 (haproxy-systemd)
   CGroup: /system.slice/haproxy.service
           ├─418 /usr/sbin/haproxy-systemd-wrapper -f /etc/haproxy/haproxy.cfg -p /run/haproxy.pid
           ├─420 /usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg -p /run/haproxy.pid -Ds
           └─421 /usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg -p /run/haproxy.pid -Ds

Apr 05 20:54:14 haproxy-loadbalancer systemd[1]: Started HAProxy Load Balancer.
Apr 05 20:54:14 haproxy-loadbalancer haproxy-systemd-wrapper[418]: haproxy-systemd-wrapper: executing /usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg -p /run/haproxy.pid -Ds


```

# exit from haproxy lxc container. On the host machine clone https://gitjub.com/nginxinc/kubernetes-ingress.git
```
git clone https://gitjub.com/nginxinc/kubernetes-ingress.git
cd deployments
```

# setup steps for ingress-controller

## Create a Namespace, a Service Account, the Default Secret and the Customization Config Map.

1. Create a namespace and a service account for the Ingress Controller
```
kubectl apply -f common/ns-and-sa.yaml
```

2. Create a secret with a TLS certificate and a key for the default server in NGINX
```
kubectl apply -f common/default-server-secret.yaml
```

3. Create a config map for customizing NGINX configuration
```
kubectl apply -f common/nginx-config.yaml
```
## Configure RBAC

1. If RBAC is enabled in your cluster, create a cluster role and bind it to the service account, created in Step 1.
```
kubectl apply -f rbac/rbac.yaml
```

### Below are two options for deploying the Ingress controller:
-- Deployment. Use a Deployment if you plan to dynamically change the number of Ingress controller replicas.
-- DaemonSet.  Use a DaemonSet for deploying the Ingress controller on every node or a subset of nodes.

## Create a Deployment

### For NGINX run:
```
kubectl apply -f deployment/nginx-ingress.yaml
```

### For NGINX Plus, run
```
kubectl apply -f deployment/nginx-plus-ingress.yaml\
```

Note: Update the `nginx-plus-ingress.yaml` with the container image that you have built.
Kubernetes will create on Ingress controller pod.

## Create a DaemonSet

### For NGINX Run
```
kubectl apply -f daemon-set/nginx-ingress.yaml
```

For NGINX Plus run
```
kubectl apply -f daemon-set/nginx-plus-ingress.yaml
```

### Note: Update the `nginx-plus-ingress.yaml` with the container image that you have built.
Kubernetes will create an Ingress controller pod on every node of the cluster.

# Running as a DaemonSet check the namespace resources
```
┌─[andre@optiplex7020]─[~/kubernetes/kubernetes-ingress/deployments]
└──╼ $kubectl -n nginx-ingress get all -o wide
NAME                      READY   STATUS    RESTARTS   AGE   IP           NODE       NOMINATED NODE   READINESS GATES
pod/nginx-ingress-6wvpc   1/1     Running   0          86s   10.244.1.4   kworker1   <none>           <none>
pod/nginx-ingress-k7z6v   1/1     Running   0          86s   10.244.3.7   kworker3   <none>           <none>
pod/nginx-ingress-klx25   1/1     Running   0          86s   10.244.2.8   kworker2   <none>           <none>

NAME                           DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE   CONTAINERS      IMAGES                     SELECTOR
daemonset.apps/nginx-ingress   3         3         3       3            3           <none>          86s   nginx-ingress   nginx/nginx-ingress:edge   app=nginx-ingress

```





