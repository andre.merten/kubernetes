### Step 1: Dump the descriptor as JSON to a file

```
kubectl get namespace <stuck_namespace> -o json > <stuck_namespace>.json
```

### Example

```{
    "apiVersion": "v1",
    "kind": "Namespace",
    "metadata": {
        "creationTimestamp": "2019-05-14T13:55:20Z",
        "labels": {
            "name": "logging"
        },
        "name": "logging",
        "resourceVersion": "29571918",
        "selfLink": "/api/v1/namespaces/logging",
        "uid": "e9516a8b-764f-11e9-9621-0a9c41ba9af6"
    },
    "spec": {
        "finalizers": [
            "kubernetes"
        ]
    },
    "status": {
        "phase": "Terminating"
    }
}

Remove kubernetes from the finalizers array:

{
    "apiVersion": "v1",
    "kind": "Namespace",
    "metadata": {
        "creationTimestamp": "2019-05-14T13:55:20Z",
        "labels": {
            "name": "logging"
        },
        "name": "logging",
        "resourceVersion": "29571918",
        "selfLink": "/api/v1/namespaces/logging",
        "uid": "e9516a8b-764f-11e9-9621-0a9c41ba9af6"
    },
    "spec": {
        "finalizers": [
        ]
    },
    "status": {
        "phase": "Terminating"
    }
}


```


### Step 2: Executing our cleanup command

```

kubectl replace --raw "/api/v1/namespaces/<stuck_namespace>/finalize" -f ./<stuck_namespace>.json



vagrant@dell-r710:~/kubernetes/yamls/cert-manager-demo/knsk$ kubectl replace --raw "/api/v1/namespaces/cert-manager/finalize" -f ./cert-manager.json
{"kind":"Namespace","apiVersion":"v1","metadata":{"name":"cert-manager","selfLink":"/api/v1/namespaces/cert-manager/finalize","uid":"3178f5ad-d2e8-4c1a-a4d4-c6fa5e9721d9","resourceVersion":"3105316","creationTimestamp":"2020-05-02T21:13:39Z","deletionTimestamp":"2020-05-02T23:54:22Z","annotations":{"kubectl.kubernetes.io/last-applied-configuration":"{\"apiVersion\":\"v1\",\"kind\":\"Namespace\",\"metadata\":{\"annotations\":{},\"creationTimestamp\":\"2020-05-02T21:13:39Z\",\"deletionTimestamp\":\"2020-05-02T23:54:22Z\",\"name\":\"cert-manager\",\"resourceVersion\":\"3100168\",\"selfLink\":\"/api/v1/namespaces/cert-manager\",\"uid\":\"3178f5ad-d2e8-4c1a-a4d4-c6fa5e9721d9\"},\"spec\":null,\"status\":{\"conditions\":[{\"lastTransitionTime\":\"2020-05-02T23:54:30Z\",\"message\":\"All resources successfully discovered\",\"reason\":\"ResourcesDiscovered\",\"status\":\"False\",\"type\":\"NamespaceDeletionDiscoveryFailure\"},{\"lastTransitionTime\":\"2020-05-02T23:54:30Z\",\"message\":\"All legacy kube types successfully parsed\",\"reason\":\"ParsedGroupVersions\",\"status\":\"False\",\"type\":\"NamespaceDeletionGroupVersionParsingFailure\"},{\"lastTransitionTime\":\"2020-05-02T23:54:30Z\",\"message\":\"Failed to delete all resource types, 1 remaining: conversion webhook for acme.cert-manager.io/v1alpha2, Kind=Challenge failed: Post https://cert-manager-webhook.cert-manager.svc:443/convert?timeout=30s: service \\\"cert-manager-webhook\\\" not found\",\"reason\":\"ContentDeletionFailed\",\"status\":\"True\",\"type\":\"NamespaceDeletionContentFailure\"},{\"lastTransitionTime\":\"2020-05-02T23:54:37Z\",\"message\":\"All content successfully removed\",\"reason\":\"ContentRemoved\",\"status\":\"False\",\"type\":\"NamespaceContentRemaining\"},{\"lastTransitionTime\":\"2020-05-02T23:54:30Z\",\"message\":\"All content-preserving finalizers finished\",\"reason\":\"ContentHasNoFinalizers\",\"status\":\"False\",\"type\":\"NamespaceFinalizersRemaining\"}],\"phase\":\"Terminating\"}}\n"}},"spec":{},"status":{"phase":"Terminating","conditions":[{"type":"NamespaceDeletionDiscoveryFailure","status":"False","lastTransitionTime":"2020-05-02T23:54:30Z","reason":"ResourcesDiscovered","message":"All resources successfully discovered"},{"type":"NamespaceDeletionGroupVersionParsingFailure","status":"False","lastTransitionTime":"2020-05-02T23:54:30Z","reason":"ParsedGroupVersions","message":"All legacy kube types successfully parsed"},{"type":"NamespaceDeletionContentFailure","status":"True","lastTransitionTime":"2020-05-02T23:54:30Z","reason":"ContentDeletionFailed","message":"Failed to delete all resource types, 1 remaining: conversion webhook for acme.cert-manager.io/v1alpha2, Kind=Challenge failed: Post https://cert-manager-webhook.cert-manager.svc:443/convert?timeout=30s: service \"cert-manager-webhook\" not found"},{"type":"NamespaceContentRemaining","status":"False","lastTransitionTime":"2020-05-02T23:54:37Z","reason":"ContentRemoved","message":"All content successfully removed"},{"type":"NamespaceFinalizersRemaining","status":"False","lastTransitionTime":"2020-05-02T23:54:30Z","reason":"ContentHasNoFinalizers","message":"All content-preserving finalizers finished"}]}



```



